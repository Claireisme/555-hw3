package edu.upenn.cis.stormlite.bolt;

import java.io.Serializable;
import java.util.ArrayList;

public class Value implements Serializable{
	
    private String key;
    private ArrayList<String> values;
    public Value(String key){
        this.key = key;
        this.values = new ArrayList<String>();
    }

    public void addvalue(String value){
        values.add(value);
    }

    public String getkey(){
        return this.key;
    }

    public ArrayList<String> getvalues(){
        return this.values;
    }
}
