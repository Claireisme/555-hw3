package edu.upenn.cis.stormlite.bolt;

import java.io.File;
import java.util.ArrayList;
import java.util.SortedMap;

import com.sleepycat.bind.EntryBinding;
import com.sleepycat.bind.serial.SerialBinding;
import com.sleepycat.bind.serial.StoredClassCatalog;
import com.sleepycat.bind.tuple.StringBinding;
import com.sleepycat.collections.StoredSortedMap;
import com.sleepycat.je.Database;
import com.sleepycat.je.DatabaseConfig;
import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;

import edu.upenn.cis.stormlite.bolt.Value;

public class DBWrapper{
    static final String CLASS_CATALOG = "java_class_catalog";
    static final String STORE = "STORE";

    private String dir;
    private Environment env;
    private StoredClassCatalog javaCatalog;
    private Database Db;

    private SortedMap<String, Value>map;

    public DBWrapper(String dir){
        this.dir = dir;
        File directory = new File(this.dir);
        if(!directory.exists()) {
        	System.out.print("directory not exist: "+directory);
            directory.mkdirs();
        }
    
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setTransactional(true);
        envConfig.setAllowCreate(true);

        this.env = new Environment(new File(this.dir), envConfig);
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setTransactional(true);
        dbConfig.setAllowCreate(true);
        Database catalogDb = env.openDatabase(null, CLASS_CATALOG, 
                                                dbConfig);

        javaCatalog = new StoredClassCatalog(catalogDb);
        this.Db =env.openDatabase(null, STORE, dbConfig);
        
        EntryBinding<String> stringBinding = new StringBinding();
        EntryBinding<Value> ValuesBinding = new SerialBinding<Value>(javaCatalog, Value.class);
        map= new StoredSortedMap<String,Value>(Db, stringBinding, ValuesBinding , true);
        
    }

    public Database getDb(){
        return this.Db;
    }

    public void close(){
        this.Db.close();
        this.javaCatalog.close();
        this.env.close();
    }

    public synchronized void addkeyvalue(String key, String value){
        if(!map.containsKey(key)){
            Value values = new Value(key);
            values.addvalue(value);
            map.put(key,values);
        }
        else{
        	Value values = map.get(key);
            values.addvalue(value);
            map.put(key,values);
        }

    }

    public Value getvalues(String key){
        
        return map.get(key);
    }

    public ArrayList<String> getkeys(){
        ArrayList<String> keys = new ArrayList<String>();
        for(String key: map.keySet()){
            keys.add(key);
        }
        return keys;
    }

}