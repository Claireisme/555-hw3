package edu.upenn.cis.stormlite.bolt;

import java.io.FileWriter;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.routers.StreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class PrintBolt implements IRichBolt {
	static Logger logger = LogManager.getLogger(PrintBolt.class);

	Fields myFields = new Fields();
	private Config config;
	private String outputdir;
	private FileWriter writer;

	String executorId = UUID.randomUUID().toString();

	@Override
	public void cleanup() {

	}

	@Override
	public boolean execute(Tuple input) {
		String key;
		String value;

		if (!input.isEndOfStream()) {
			key = input.getStringByField("key");
			value = input.getStringByField("value");

		}
		return false;
	}

	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setRouter(StreamRouter router) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return null;
	}

}