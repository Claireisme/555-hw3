package edu.upenn.cis.stormlite.spout;

import java.io.File;

public class MyFileSpout extends FileSpout{

	@Override
	public String getFilename() {
		// TODO Auto-generated method stub
        String dir;
        String inputdir;
        String filename = "";
        
        inputdir = super.getConfig().get("inputdirectory");
        if(inputdir != null && inputdir != ""){
            dir = super.getConfig().get("storagedirectory");
        }
        else{
            dir = super.getConfig().get("storagedirectory")+"/"+inputdir;
        }
        
        File folder = new File(dir);
        String[] files = folder.list();
        for(String file: files){
            String[] parts = file.split("\\.");
            if(parts.length>1){
                filename = parts[0]+"."+parts[1];
                break;
            }
            else{
                filename = parts[0];
            }
        }

        return dir+"/"+filename;
	}

}