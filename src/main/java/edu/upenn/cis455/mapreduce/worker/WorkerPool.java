package edu.upenn.cis455.mapreduce.worker;

import java.util.HashMap;

import edu.upenn.cis.stormlite.Config;

public class WorkerPool {
	private static HashMap<Integer, Worker> workerPool = new HashMap<Integer, Worker>();

	public static HashMap<Integer, Worker> getWorkerPool() {
		return workerPool;
	}

	public static void addWorker(int port, Worker worker) {
		workerPool.put(port, worker);
	}

	public static Worker getWorker(Config config) {
		System.out.println("WorkerPool getWorker() workerPool: "+workerPool);
		int port = Worker.portFromConifg(config);
		return getWorker(port);
	}

	public static Worker getWorker(int port) {
		return workerPool.get(port);
	}

	public static void shutdown() {
		for (int key : workerPool.keySet()) {
			workerPool.get(key).shutdown();
		}
		workerPool.clear();
	}

	public static synchronized void setstate(Config config, String state) {
		System.out.println("WorkerPool setstate(): "+state);
		getWorker(config).setState(state);
	}

	public static synchronized void addKeysRead(Config config) {
		getWorker(config).addKeysRead();
	}

	public static synchronized void addKeysWritten(Config config) {
		getWorker(config).addKeysRead();
	}

	public static synchronized void addresult(Config config, String result) {
		getWorker(config).addResults(result);
	}

	public static synchronized void setinputir(Config config, String inputdir) {
		getWorker(config).setInputdir(inputdir);
	}

	public static synchronized void setoutputdir(Config config, String outputdir) {
		getWorker(config).setOutputdir(outputdir);
	}

	public static synchronized void setjob(Config config, String job) {
		getWorker(config).setJob(job);
	}

	public static synchronized void setmaster(Config config, String master) {
		getWorker(config).setMaster(master);
	}
}