package edu.upenn.cis455.mapreduce.worker;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;

public class Worker extends Thread {
	static final Logger logger = LogManager.getLogger(Worker.class);

	private int port;
	private String master;
	private String job;
	private String inputdir;
	private String outputdir;
	private String state = "idle"; // mappping, waiting, reducing, idle
	private int keysRead;
	private int keysWritten;
	private ArrayList<String> results = new ArrayList<String>();
	private boolean isdone = false;

	public Worker(int port) {
		this.port = port;
	}

	public Worker(String master, int port, String inputdir, String outputdir) {
		this.master = master;
		this.port = port;
		this.inputdir = inputdir;
		this.outputdir = outputdir;
	}

	public Worker(String master, int port, String job, String inputdir, String outputdir) {
		this.master = master;
		this.port = port;
		this.inputdir = inputdir;
		this.outputdir = outputdir;
		this.job = job;
	}

	public String getJob() {
		return this.job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public void setState(String state) {
		this.state = state;
		if (this.state.equals("idle")) {
			this.keysRead = 0;
			this.keysWritten = 0;
			this.results = new ArrayList<String>();
		}
	}

	public String getstate() {
		return this.state;
	}

	public String getResult() {
		ArrayList<String> temp = new ArrayList<String>();

		int count = 0;
		for (String result : this.results) {
			temp.add(result);
			count += 1;
			if (count == 100)
				break;
		}

		String output = "is";
		for (String in : temp) {
			output += in + ",";
		}

		return output;
	}

	@Override
	public void run() {
		while (!isdone) {
			String url_str = "http://" + master + "/workerstatus";
			String params = "port=" + port + "&status=" + state + "&job=" + job + "&keysRead=" + keysRead
					+ "&keysWritten=" + keysWritten + "&results=" + results;

			URL url = null;

			try {
				url = new URL(url_str + "?" + params);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			HttpURLConnection connection = null;
			try {
				connection = (HttpURLConnection) url.openConnection();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				connection.setRequestMethod("GET");
			} catch (ProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				connection.getResponseMessage();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			connection.disconnect();
		}

		try {
			sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static int portFromConifg(Config config) {
		if (!config.containsKey("workerList"))
			throw new RuntimeException("Worker doesn't have list of worker IP addresses/ports");

		if (!config.containsKey("workerIndex"))
			throw new RuntimeException("Worker doesn't know its worker ID");

		String[] addresses = WorkerHelper.getWorkers(config);
		String myAddress = addresses[Integer.valueOf(config.get("workerIndex"))];
		URL url = null;
		try {
			url = new URL(myAddress);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return url.getPort();
	}

	public void shutdown() {
		this.isdone = true;
	}

	public String getInputdir() {
		return inputdir;
	}

	public void setInputdir(String inputdir) {
		this.inputdir = inputdir;
	}
	
	public void setMaster(String master) {
		this.master = master;
	}

	public void addKeysRead() {
		this.keysRead++;
	}

	public void addKeysWritten() {
		this.keysWritten++;
	}

	public String getOutputdir() {
		return outputdir;
	}

	public void addResults(String result) {
		this.results.add(result);
	}

	public void setOutputdir(String outputdir) {
		this.outputdir = outputdir;
	}

	public int getPort() {
		return port;
	}

	public String getMaster() {
		return master;
	}

	public int getKeysRead() {
		return keysRead;
	}

	public int getKeysWritten() {
		return keysWritten;
	}

	public ArrayList<String> getResults() {
		return results;
	}
}
