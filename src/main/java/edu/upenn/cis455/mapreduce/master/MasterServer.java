package edu.upenn.cis455.mapreduce.master;

import static spark.Spark.get;
import static spark.Spark.halt;
import static spark.Spark.port;
import static spark.Spark.post;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.MapBolt;
import edu.upenn.cis.stormlite.bolt.PrintBolt;
import edu.upenn.cis.stormlite.bolt.ReduceBolt;
import edu.upenn.cis.stormlite.distributed.WorkerHelper;
import edu.upenn.cis.stormlite.distributed.WorkerJob;
import edu.upenn.cis.stormlite.spout.FileSpout;
import edu.upenn.cis.stormlite.spout.MyFileSpout;
import edu.upenn.cis.stormlite.tuple.Fields;

public class MasterServer {
	private static final String WORD_SPOUT = "WORD_SPOUT";
	private static final String COUNT_BOLT = "COUNT_BOLT";
	private static final String PRINT_BOLT = "PRINT_BOLT";

	static Logger logger = LogManager.getLogger(MasterServer.class);

	private static HashMap<String, HashMap<String, String>> workers = new HashMap<String, HashMap<String, String>>();

	public static void registerStatusPage() {
		System.out.println("registerStatusPage");
		get("/status", (request, response) -> {
			response.type("text/html");
			StringBuilder builder = new StringBuilder();
			builder.append("<html><head><title>Master</title></head><body><table>");
			for (String key : workers.keySet()) {
				builder.append("<tr><td>IP:port " + key + "</td>");
				builder.append("<td> status " + workers.get(key).get("status") + "</td>");

				builder.append("<td> job " + workers.get(key).get("job") + "</td>");
				builder.append("<td> keysRead " + workers.get(key).get("keysRead") + "</td>");
				builder.append("<td> keysWritten " + workers.get(key).get("keysWritten") + "</td>");
				builder.append("<td> results " + workers.get(key).get("results") + "</td></tr>");

			}
			builder.append("</table>");
			builder.append("<form method=\"POST\" action=\"/submit\">");
			builder.append("job : <input type=\"text\" name=\"job\"/><br/>");
			builder.append("inputdirectory : <input type=\"text\" name=\"inputdirectory\"/><br/>");
			builder.append("outputdirectory : <input type=\"text\" name=\"outputdirectory\"/><br/>");
			builder.append("mapExecutors : <input type=\"text\" name=\"mapExecutors\"/><br/>");
			builder.append("reduceExecutors : <input type=\"text\" name=\"reduceExecutors\"/><br/>");
			builder.append("<input type=\"submit\" value=\"submmiting jobs\"></form>");
			builder.append("</body></html>");

			return builder.toString();
		});
		get("/status", (request, response) -> {
			response.type("text/html");
			return ("<html><body>Hi, I am the master!</body></html>");
		});

	}

	private static void Shutdown() {
		System.out.println("MasterServer shutdown");
		get("/shutdown", (request, response) -> {
			StringBuilder sb = new StringBuilder();
			sb.append("<html><head>The Master server shutdown</head></html>");

			Config config = new Config();
			List<String> workerList = new ArrayList<String>();
			for (String key : workers.keySet()) {
				workerList.add(key);
			}
			config.put("workerList", workerList.toString());

			String[] workernames = WorkerHelper.getWorkers(config);
			int i = 0;
			for (String workeradd : workernames) {
				config.put("workerIndex", String.valueOf(i++));
				sendJob(workeradd, "GET", config, "shutdown", "");
			}
			return sb.toString();
		});
	}

	private static void WorkerStatusPage() {
		get("/workerstatus", (request, response) -> {
			String ip = request.ip();
			if (request.params() == null) {
				halt(400);
			}
			String port = request.queryParams("port");
			String ip_port = ip + ":" + port;
			String status = request.queryParams("status");
			String job = request.queryParams("job");
			String keysRead = request.queryParams("keysRead");
			String keysWritten = request.queryParams("keysWritten");
			String results = request.queryParams("results");
			HashMap<String, String> state = new HashMap<String, String>();
			state.put("status", status);
			state.put("job", job);
			state.put("keysRead", keysRead);
			state.put("keysWritten", keysWritten);
			state.put("results", results);

			workers.put(ip_port, state);
			response.status(HttpURLConnection.HTTP_OK);

			return null;
		});

	}

	private static void createSampleMapReduce(Config config, String job, String mapExecutors, String reduceExecutors,
			String inputdir, String outputdir) {
		// Job name
		config.put("job", job);

		// Class with map function
		config.put("mapClass", job);
		// Class with reduce function
		config.put("reduceClass", job);

		// Numbers of executors (per node)
		config.put("spoutExecutors", "1");
		config.put("mapExecutors", mapExecutors);
		config.put("reduceExecutors", reduceExecutors);

		// directory
		config.put("inputdirectory", inputdir);
		config.put("outputdirectory", outputdir);
		config.put("storagedirectory", "./555-hw3/store");
	}

	private static void Submit() {
		String FILE_SPOUT = "FILE_SPOUT";
		String MAP_BOLT = "MAP_BOLT";
		String REDUCE_BOLT = "REDUCE_BOLT";
		String PRINT_BOLT = "PRINT_BOLT";

		post("/submit", (request, response) -> {
			System.out.println("submit");
			String job = request.queryParams("job");
			String inputdir;
			String outputdir;

			inputdir = request.queryParams("inputdirectory");
			outputdir = request.queryParams("outputdirectory");
			String mapExecutors = request.queryParams("mapExecutors");
			String reduceExecutors = request.queryParams("reduceExecutors");

			ArrayList<String> workerList = new ArrayList<String>();
			if (workers.isEmpty()) {
				return "no worker";
			}

			for (String key : workers.keySet()) {
				if (workers.get(key).get("status").equals("idle")) {
					workerList.add(key);
				}
			}

			if (workerList.isEmpty()) {
				return "no idle worker";
			}

			Config config = new Config();

			config.put("workerList", workerList.toString());
			config.put("master", "127.0.0.1:8080");

			logger.info("************ Creating the job request ***************");
			System.out.println("************ Creating the job request ***************");
			createSampleMapReduce(config, job, mapExecutors, reduceExecutors, inputdir, outputdir);

			FileSpout spout = new MyFileSpout();
			MapBolt bolt = new MapBolt();
			ReduceBolt bolt2 = new ReduceBolt();
			PrintBolt printer = new PrintBolt();

			TopologyBuilder builder = new TopologyBuilder();

			// Only one source ("spout") for the words
			builder.setSpout(WORD_SPOUT, spout, Integer.valueOf(config.get("spoutExecutors")));

			// Parallel mappers, each of which gets specific words
			builder.setBolt(MAP_BOLT, bolt, Integer.valueOf(config.get("mapExecutors"))).fieldsGrouping(WORD_SPOUT,
					new Fields("value"));

			// Parallel reducers, each of which gets specific words
			builder.setBolt(REDUCE_BOLT, bolt2, Integer.valueOf(config.get("reduceExecutors"))).fieldsGrouping(MAP_BOLT,
					new Fields("key"));

			// Only use the first printer bolt for reducing to a single point
			builder.setBolt(PRINT_BOLT, printer, 1).firstGrouping(REDUCE_BOLT);

			Topology topo = builder.createTopology();

			WorkerJob workerJob = new WorkerJob(topo, config);

			ObjectMapper mapper = new ObjectMapper();
			mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);

			try {
				String[] workers = WorkerHelper.getWorkers(config);

				int i = 0;
				for (String dest : workers) {
					config.put("workerIndex", String.valueOf(i++));
					if (sendJob(dest, "POST", config, "definejob",
							mapper.writerWithDefaultPrettyPrinter().writeValueAsString(workerJob))
									.getResponseCode() != HttpURLConnection.HTTP_OK) {
						throw new RuntimeException("Job definition request failed");
					}
				}
				for (String dest : workers) {
					if (sendJob(dest, "POST", config, "runjob", "").getResponseCode() != HttpURLConnection.HTTP_OK) {
						throw new RuntimeException("Job execution request failed");
					}
				}
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.exit(0);
			}

			response.redirect("/status");
			return null;
		});
	}

	private static HttpURLConnection sendJob(String dest, String reqType, Config config, String job, String parameters)
			throws IOException {
		URL url = new URL(dest + "/" + job);

		logger.info("Sending request to " + url.toString());

		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod(reqType);

		if (reqType.equals("POST")) {
			conn.setRequestProperty("Content-Type", "application/json");

			OutputStream os = conn.getOutputStream();
			byte[] toSend = parameters.getBytes();
			os.write(toSend);
			os.flush();
		} else
			conn.getOutputStream();

		return conn;
	}

	/**
	 * The mainline for launching a MapReduce Master. This should handle at least
	 * the status and workerstatus routes, and optionally initialize a worker as
	 * well.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 1) {
			System.out.println("Usage: MasterServer [port number]");
			System.exit(1);
		}

		int myPort = Integer.valueOf(args[0]);
		port(myPort);

		System.out.println("Master node startup, on port " + myPort);

		// TODO: you may want to adapt parts of
		// edu.upenn.cis.stormlite.mapreduce.TestMapReduce here

		registerStatusPage();

		// route handler for /workerstatus reports from the workers
//		WorkerStatusPage();
//		Submit();
//		Shutdown();
	}

}
